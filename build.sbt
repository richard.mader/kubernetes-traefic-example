name := "example-service"
ThisBuild / organization := "example-service"
ThisBuild / scalaVersion := "2.12.8"
ThisBuild / version := "0.1.0-SNAPSHOT"

// Services //
lazy val root = (project in file("."))
  .enablePlugins(PlayScala, JavaServerAppPackaging)
  .settings(
    libraryDependencies += guice,
    dockerExposedPorts += 9000
  )
