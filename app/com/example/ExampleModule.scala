package com.example

import play.api.inject.{Binding, Module}
import play.api._

class ExampleModule extends Module {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq()
}
