package com.example

import java.util.UUID
import javax.inject.Inject
import play.api.mvc.{AbstractController, ControllerComponents}

class ExampleService @Inject() (cc: ControllerComponents) extends AbstractController(cc) {
  val InstanceId = UUID.randomUUID

  val root = Action { request =>
    Ok(s"Hello from pod $InstanceId")
  }
}
